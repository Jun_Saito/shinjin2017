#coding: utf-8
from pyknp import KNP 

knp = KNP(jumanpp=True)
result = knp.parse("彼が借りたのは英語の本だ")

#文節のリスト

for bnst in result.bnst_list():
    a = 0
    parent = bnst.parent
    if parent is not None:
        for x in bnst.mrph_list():
            if x.hinsi=="動詞" or x.hinsi=="形容詞":
                a += 1
        for x in parent.mrph_list():
            if x.hinsi=="名詞":
                a += 1
        if a == 2 and parent.bnst_id - bnst.bnst_id != 1:
            child_rep = " ".join(mrph.repname for mrph in bnst.mrph_list())
            parent_rep = " ".join(mrph.repname for mrph in parent.mrph_list())
            print(child_rep, "->", parent_rep)
