print('4-1.')
def f(n):
    if n==1 or n==2:
        return 1
    return f(n-2)+f(n-1)

import sys
a = sys.argv
n = int(a[1])
print(f(n))

print('\n4-2.')
a = [1, 1]
for i in range(2, n+1):
    a.append(a[i-2]+a[i-1])
print(a[n-1])

print('\n4-3.')
print('再帰を使ったプログラムの計算量は指数関数で増えていくが、再帰を用いなければ計算量はnに比例する。つまり再帰を使わないほうが良い。')
