def generate_perm(n):
    assert isinstance(n, int)
    assert n > 0

    perm = [None for j in range(n)]

    m = 0

    while True:
        x = perm[m] or 0

        for y in range(x+1, n+1):
            if y in perm:
                continue

            perm[m] = y

            if m + 1 < n:
                m += 1
            else:
                yield perm
                perm[m] = None
                m -= 1
            break
        else:
            perm[m] = None
            m -= 1

            if m == -1:
                break

def check(perm):
    for y in range(len(perm)):
        if conflict(perm, perm[y], y):
            return False
    return True

def conflict(perm, x, y):
    for y1 in range(y):
        x1 = perm[y1]
        if x1 - y1 == x - y or x1 + y1 == x + y:
            return True
    return False

c = 0
for perm in generate_perm(8):
    if check(perm):
        c += 1
print(c)
