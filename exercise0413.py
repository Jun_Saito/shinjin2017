print("e1")
a = [1, 2, 3]
b = a[:]
print(b)

print("\ne2")
a = []
for i in range(1,31):
    a.append(i)
print(a)
for i in range(0, 30):
    if a[i]%3==0:
        a[i] = 0
print(a)

print("\ne3")
import sys
l = sys.argv
i = int(l[1])
a = ["0", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
print(a[i])

print("\ne4")
a = "Yakult"
b = "Swallows"
c = a[0] + b[0]
print(c)

print("\ne5")
a = "yakultswallows"
b = []
for i in range(0, len(a)):
    c = 0
    for j in range(i):
        if a[j]==a[i]:
            c = 1
    if c==0:
        b.append(a[i])
print(b)

print("\ne6")
a = "stressed"
print(a[::-1])

print("\ne7")
a = "パタトクカシーー"
b = ""
for i in range(0, 8):
    if i%2==0:
        b += a[i]
print (b)

print("\ne8")
a = "パトカー"
b = "タクシー"
c = ""
for i in range(0,4):
    c += a[i] + b[i]
print(c)

print("\ne9")
a = "Hi He Lied Because Boron Could Not Oxidize Fluorine. New Nations Might Also Sign Peace Security Clause. Arthur King Can."
b = a.split(' ')
c = ""
for i in range(0, 20):
    if i==0 or i==4 or i==5 or i==6 or i==7 or i==8 or i==14 or i==15 or i==18:
        c += b[i][0]
    else:
        c += b[i][0:2]
print (c)

print("\ne10")
a = ['eggs', 'spam', 'spam', 'bacon']
freq = {}
for i in range(0, len(a)):
    if a[i] not in freq:
        freq[a[i]] = 1
    else:
        freq[a[i]] += 1
for key in freq:
        print(key, freq[key])

print("\ne11")
a = ['f', 'o', 't', 'b', 'a', 'l']
b = {}
for i in range(0, 6):
    if a[i] not in b:
        b[a[i]] = i
c = 'football'
d = []
for i in range(0, 8):
        d.append(b[c[i]])
        print (d[i], end="")
print("\n")
for i in range(0, 8):
    for j in range(0, 6):
        if d[i]==b[a[j]]:
            print (a[j], end="")
            break
print("\n")
        
