#coding: utf-8                                                                      
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む                     
    data += line
    
    if line.strip() == "EOS": # 1文が終わったら解析                                

        result = jumanpp.result(data)
        for x, y in zip(result.mrph_list(), result.mrph_list()[1:]):
            if x.bunrui == "サ変名詞" and (y.genkei == "する" or y.genkei == "できる"):
                print(x.midasi, y.midasi)
        data = ""
