#coding: utf-8                                                                      
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む                     
    data += line
    
    if line.strip() == "EOS": # 1文が終わったら解析                                

        result = jumanpp.result(data)
        for x, y, z in zip(result.mrph_list(), result.mrph_list()[1:], result.mrph_list()[2:]):
            if x.hinsi == "名詞" and y.midasi == "の" and z.hinsi == "名詞":
                print(x.midasi, y.midasi, z.midasi)
        data = ""
