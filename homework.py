print("1.")
for i in range(1, 31):
    print(i)

print("\n2.")
for i in range(2, 31, 2):
    print(i)

print("\n3.")
sum = 0
for i in range(2, 31, 2):
    sum += i 
print(sum)

print("\n4.")
product = 1
for i in range(2, 31, 2):
    product *= i
print(product)

print("\n5.")
for i in range(1, 10):
    for j in range(1, 10):
        print(i, "*", j, "=",  i*j)

print("\n6.")
for i in range(1, 31):
    if i%3==0 and i%5==0:
        print("FizzBuzz")
    elif i%3==0:
        print("Fizz")
    elif i%5==0:
        print("Buzz")
    else:
        print(i)

print("\n7.")
for i in range(2, 1001):
    for j in range(2, i+1):
        if j!=i and i%j==0:
            break
        if j==i:
            print(i)
        
