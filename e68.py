#coding: utf-8
from pyknp import KNP 

# インスタンスを作成
knp = KNP(jumanpp=True)

# 文を解析し、解析結果を Python の内部構造に変換して result に格納
import sys
input_sentence = sys.stdin.readline()
result = knp.parse(input_sentence)

# 解析結果へのアクセス
# 文節、基本句、形態素の3階層からなり、resultから各階層へアクセスできる。
# また各階層からは下位の階層へアクセスできる。
# 1.各文節へのアクセス: bnst_list()
for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()))
print("\n\n")
