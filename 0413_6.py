import time
t0 = time.clock()

a = []
for i in range(2, 10001):
    a.append(i)
for i in range(0, 9998):
    if a[i]>100:
        break
    for j in range(i+1, 9998):
        if a[i]!=0 and a[j]%a[i]==0:
            a[j] = 0
b = []
for i in range(0, len(a)-1):
    if a[i]!=0:
        b.append(a[i])
print(b)

t1 = time.clock()
print("dt="+str(t1-t0)+"[s]")

t0 = time.clock()

for i in range(2, 10001):
    for j in range(2, i+1):
        if j!=i and i%j==0:
            break
        if j==i:
            print(i)

t1 = time.clock()
print("dt="+str(t1-t0)+"[s]")
