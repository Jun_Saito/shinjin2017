with open("doc0000000000.txt") as my_file:
    freq = {}
    for line in my_file:
        if line.startswith("<PAGE URL=")==False and line.startswith("</PAGE>")==False: 

            string = line.lower()

            unigram_list = string.split()
            for x in unigram_list:
                if x in freq:
                    freq[x] += 1
                else:
                    freq[x] = 1

            bigram_list = zip(unigram_list, unigram_list[1:])
            for x in bigram_list:
                if x in freq:
                    freq[x] += 1
                else:
                    freq[x] = 1

    sentence = "The man is in the house."
    list0 = sentence.lower()
    list1 = list0.split() 
    p = freq[list1[0]] / sum(freq.values())
    for x, y in zip(list1, list1[1:]):
        p *= freq[x, y] / freq[x]
    print(p)
    
