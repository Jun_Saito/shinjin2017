#coding: utf-8                                                                      
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

freq = {}
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む                     
    data += line
    
    if line.strip() == "EOS": # 1文が終わったら解析                                

        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if mrph.genkei in freq:
                freq[mrph.genkei] += 1
            else:
                freq[mrph.genkei] = 1
        data = ""

for word,cnt in sorted(freq.items(), key=lambda x:x[1], reverse=True):
    print(word, cnt)
