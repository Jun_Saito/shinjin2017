#coding: utf-8                                                                      
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

word = 0
jutsugo = 0
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む                     
    data += line
    
    if line.strip() == "EOS": # 1文が終わったら解析                                

        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            word += 1
            if mrph.hinsi == "動詞" or mrph.hinsi == "形容詞":
                jutsugo += 1
        data = ""

print(100 * jutsugo / word, '%')
