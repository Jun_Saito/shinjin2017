def bubble(a):
    for i in range(0, len(a)-1):
        for j in range(0, len(a)-1-i):
            if a[j] > a[j+1]:
                b = a[j]
                a[j] = a[j+1]
                a[j+1] = b
    return a
                
def quick(a):
    if len(a) < 2:
        return a
    pivot = a[0]
    a_rest =  a[1:]
    smaller = [x for x in a_rest if x < pivot]
    larger = [x for x in a_rest if x >= pivot]
    return  quick(smaller) + [pivot] + quick(larger)

def merge(a):                                   
    mid = len(a)                                
    if mid > 1:                                    
        left = merge(a[:int(mid/2)])              
        right = merge(a[int(mid/2):])            
        a = []
        while len(left) != 0 and len(right) != 0:
            if left[0] < right[0]:           
                a.append(left.pop(0))   
            else:                                  
                a.append(right.pop(0))          
        if len(left) != 0:                         
            a.extend(left)                    
        elif len(right) != 0:                       
            a.extend(right)                     
    return a                                    


l = [8, 4, 3, 7, 6, 5, 2, 1]
print (l)
print("バブルソート", bubble(l))
print("クイックソート", quick(l))
print("マージソート", merge(l))
