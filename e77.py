#coding: utf-8
from pyknp import KNP 

knp = KNP(jumanpp=True)
result = knp.parse("君の明日の運勢は悪い.")

for x, y in zip(result.bnst_list(), result.bnst_list()[1:]):
    if len(x.mrph_list()) > 1:
        if x.mrph_list()[-2].hinsi == "名詞":
            if x.mrph_list()[-1].midasi == "の":
                if y.mrph_list()[0].hinsi == "名詞":
                    if x.parent != y:
                        print(x.mrph_list()[-2].midasi, x.mrph_list()[-1].midasi, y.mrph_list()[0].midasi)
