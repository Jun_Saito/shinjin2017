#coding: utf-8                                                                     
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""

for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            a = 0
            for x in bnst.mrph_list():
                if x.hinsi == "名詞":
                    if a==1:
                        break
                    for y in bnst.mrph_list():
                        if "接尾" in y.fstring:
                            print("" .join(z.repname for z in bnst.mrph_list()))
                            a = 1
                            break
        data = ""
